
//let playerlist = [];

//Diese Map ist für 4 Spieler ausgelegt
let playerFigureCount = 4;

//Gesamte größe der Map
let gameFieldSize = 40;
//Mapgröße von einem Spieler zum nächsten
let mapSizePart = gameFieldSize / playerFigureCount;

let gameFields;

let currentPlayerRolls = 0;
let lastPlayerRoll = 0;

let playerMustRoll;

module.exports = class GameHandler{

    constructor(gameSession, playerlist) {
        this.playerlist = playerlist;
        this.gameSession = gameSession;

        //Notwendige Inizierungen vor dem Start
        this.initGameField();
        this.initPlayer();

        this.start();
    };

    start(){
        /*gameFields[0].owner = "admin";
        gameFields[5].owner = "admin";
        gameFields[20].owner = "admin";
        gameFields[30].owner = "admin";*/


        //Spieler benötigen Grundlegende Informationen über die Map und deren Figuren
        this.sendPlayersGameFieldUpdate();

        //Zufälliger Spieler wird ausgewählt. Dieser beginnt mit einem Zug
        let answerObject = {};
        answerObject.command = "sendToGameSession";
        answerObject.action = "roll";
        answerObject.sessionID = this.gameSession.sessionID;
        let randomPlayer = this.selectRandomPlayer();
        answerObject.playername = randomPlayer.getPlayerName();

        randomPlayer.setRolls(3);

        for (let i = 0; i < this.getPlayerList().length; i++) {
            this.getPlayerList()[i].getConnection().sendUTF(JSON.stringify(answerObject));
        }

        //TO-DO Diese Nachricht an alle Spieler senden. SessionID nicht random erzeugen sonder hochzählen.
    }

    onMessageReceived(receivementData, connection){
        let action = receivementData.action;

        if(this.getPlayerList()[this.currentPlayer].getConnection() === connection) {
            let player = this.getPlayerList()[this.currentPlayer];

            if(action === "roll"){

                    //prüfen ob der Spieler schon eine Figur auf dem Spielfeld hat
                    let playerOnGameField = false;
                    for (let i = 0; i < gameFields.length; i++) {
                        if(gameFields[i].owner === player.getPlayerName()){
                            playerOnGameField = true;
                        }
                    }

                    let playerOnBaseField;
                    if(!this.checkBasePossibleRolls(player)){
                        playerOnBaseField = false;
                    }else{
                        playerOnBaseField = true;
                    }

                    //Es muss geprüft werden ob der Spieler überhaupt noch einen Zug durchführen kann. Wenn nicht, ist entweder der nächste Spieler dran oder Wenn noch Figuren
                    //am Start verfügbar sind, muss er drei mal Würfeln.

                    if(playerOnGameField === true || playerOnBaseField === true){
                        //Spieler hat bereits eine Figur auf dem Spielfeld. Dieser darf nur einmal Würfeln
                        //console.log("Spieler hat bereits eine Figur auf dem Spielfeld. Dieser darf nur einmal Würfeln");

                        let roll = this.roll();

                        lastPlayerRoll = roll;
                        playerMustRoll = false;
                        console.log("Spieler hat eine " + roll + " gewürfelt");

                        if(roll === 6 && this.countPlayersFiguresOnGameField(player) < 4){
                            gameFields[player.getStartPoint()].owner = player.getPlayerName();

                            //Nachricht an alle Spieler, dass sich das Spielfeld verändert hat
                            this.sendPlayersGameFieldUpdate();

                            //Spieler muss erneut würfeln
                            let answerObject = {};
                            answerObject.command = "sendToGameSession";
                            answerObject.action = "roll";
                            answerObject.rolled = roll;
                            answerObject.sessionID = this.gameSession.sessionID;
                            answerObject.playername = player.getPlayerName();

                            for (let i = 0; i < this.getPlayerList().length; i++) {
                                this.getPlayerList()[i].getConnection().sendUTF(JSON.stringify(answerObject));
                            }
                        }else{
                            if(this.playerCanDoAnything(player, roll)){
                                console.log("Spieler kann einen Zug durchführen");
                                let answerObject = {};
                                answerObject.command = "sendToGameSession";
                                answerObject.action = "playerDecideMovement";
                                answerObject.sessionID = this.gameSession.sessionID;
                                answerObject.rolled = roll;

                                player.getConnection().sendUTF(JSON.stringify(answerObject));
                            }else{
                                console.log("Spieler kann keinen Zug durchführen. Nächster Spieler");
                                this.setNextPlayer();
                            }
                        }

                    }
                    else{
                        player.setRolls(3);
                        //Spieler hat keine Figur auf dem Spielfeld. Er darf drei mal würfeln
                        console.log("Spieler hat keine Figur auf dem Spielfeld. Er darf drei mal würfeln");
                        if(currentPlayerRolls < player.getRolls()){
                            //Spieler darf würfeln
                            let roll = this.roll();
                            console.log(roll);
                            //roll = 6;

                            if(roll === 6){
                                //Spieler hat eine 6 gewürfelt. Der Charakter darf das Spielfeld verlassen
                                console.log("Spieler hat eine 6 gewürfelt. Der Charakter darf das Spielfeld verlassen");
                                gameFields[player.getStartPoint()].owner = player.getPlayerName();

                                //Nachricht an alle Spieler, dass sich das Spielfeld verändert hat
                                this.sendPlayersGameFieldUpdate();

                                //Wir warten kurz, bis es weiter geht
                                //setTimeout(null, 3000);

                                //Spieler muss erneut würfeln
                                this.sendRollCommand();


                            }else{
                                //Spieler hat keine 6 gewürfelt.
                                console.log("Spieler hat keine 6 gewürfelt.");

                                this.sendRollCommand()
                            }
                            currentPlayerRolls++;
                        }else{
                            //Spieler darf nicht mehr würfeln
                            this.setNextPlayer();
                        }
                    }
            }
            else if(action === "playerDecideMovement"){

                if(!playerMustRoll){
                    let gameFieldId = receivementData.playerGameFieldId;
                    console.log("GameFieldID: " + gameFieldId);


                    //Prüfen ob eine Figur, die am Startpunkt liegt, bewegt werden muss
                    if(gameFields[player.getStartPoint()].owner === player.getPlayerName()){
                        //Ja, muss es
                        if(gameFieldId === player.getStartPoint()){
                            //Gültiger Zug

                            if(gameFields[gameFieldId + lastPlayerRoll].owner !== player.getPlayerName()) {
                                gameFields[gameFieldId].owner = null;
                                gameFields[gameFieldId + lastPlayerRoll].owner = player.getPlayerName();

                                this.setNextPlayer();
                            }else{
                                //Es ist ein Spieler auf dem Feld mit der gewürfelten Reichweite. Suche eine andere Figur aus

                                //Er soll nochmal entscheiden
                                let answerObject = {};
                                answerObject.command = "sendToGameSession";
                                answerObject.action = "playerDecideMovement";
                                answerObject.sessionID = this.gameSession.sessionID;
                                answerObject.message = "Eine andere Figur von dir ist in der Reichweite schon plaziert";

                                player.getConnection().sendUTF(JSON.stringify(answerObject));
                            }
                        }else{
                            //Ungültiger Zug
                            let answerObject = {};
                            answerObject.command = "sendToGameSession";
                            answerObject.action = "playerDecideMovement";
                            answerObject.sessionID = this.gameSession.sessionID;
                            answerObject.message = "Du musst deine Figur am Startpunkt bewegen";

                            player.getConnection().sendUTF(JSON.stringify(answerObject));
                        }
                    }
                    else{
                        //Nein, muss es nicht

                        //hier prüfen ob es sich um eine bewegung im Haus oder auf dem generellen Spielfeld handelt
                        let gameFieldType = receivementData.gameFieldType;
                        if(gameFieldType === 0){
                            //Es handelt sich um das Spielfeld
                            if(gameFields[gameFieldId].owner === player.getPlayerName()){
                                //Das Feld gehört dem Spieler. Alles gut

                                //Prüfen ob Spieler eine Figur in Reichweite seines Zuges hat


                                let calculatedField;
                                if((gameFieldId + lastPlayerRoll) > gameFieldSize - 1){
                                    calculatedField = (gameFieldId + lastPlayerRoll) - gameFieldSize;
                                }else{
                                    calculatedField = (gameFieldId + lastPlayerRoll);
                                }

                                if(gameFields[calculatedField].owner !== player.getPlayerName()){
                                    //Es ist kein eigener Spieler auf dem Feld mit der gewürfelten Reichweite

                                    if(player.getStartPoint() !== 0){
                                        //Gilt für alle Spieler außer Spieler 1

                                        if(calculatedField > player.getBaseEntryPoint() && gameFieldId < player.getStartPoint() && (calculatedField - player.getBaseEntryPoint() <= 4)){
                                            //Figur landet im Häusschen
                                            let hausschritt = calculatedField - player.getBaseEntryPoint();

                                            gameFields[gameFieldId].owner = null;
                                            player.getBasePoints()[hausschritt-1].owner = player.getPlayerName();

                                            this.setNextPlayer();
                                        }
                                        else{
                                            //Normaler Fortschitt

                                            gameFields[gameFieldId].owner = null;
                                            gameFields[calculatedField].owner = player.getPlayerName();

                                            this.setNextPlayer();
                                        }
                                    }
                                    else{
                                        //Gilt für Spieler 1
                                        let calculatedField = (gameFieldId + lastPlayerRoll);//43

                                        if(calculatedField > player.getBaseEntryPoint() && gameFieldId > player.getStartPoint()){
                                            //Figur müsste ins Häusschen. Prüfen ob gewürfelte Zahl zu hoch ist

                                            if((calculatedField - player.getBaseEntryPoint() <= 4)){
                                                //Figur landet im Häusschen

                                                //prüfen ob sich bereits eine Figur auf dem Feld befindet
                                                if(player.getBasePoints()[(calculatedField - player.getBaseEntryPoint()) - 1].owner == null){
                                                    //Eine andere Figur ist bereits auf dem Feld
                                                    let housemove = calculatedField - player.getBaseEntryPoint();

                                                    gameFields[gameFieldId].owner = null;
                                                    player.getBasePoints()[housemove-1].owner = player.getPlayerName();

                                                    this.setNextPlayer();
                                                }else{
                                                    //Keine andere Figur ist auf dem Feld. Der Zug ist möglich
                                                    console.log("Zug ist nicht möglich");
                                                    let answerObject = {};
                                                    answerObject.command = "sendToGameSession";
                                                    answerObject.action = "playerDecideMovement";
                                                    answerObject.sessionID = this.gameSession.sessionID;
                                                    answerObject.message = "Eine andere Figur ist auf dem Feld. Zug nicht möglich";

                                                    player.getConnection().sendUTF(JSON.stringify(answerObject));
                                                }
                                            }else{
                                                //Figur würde außerhalb des Feldes landen. Fehler
                                                //Er soll nochmal entscheiden
                                                let answerObject = {};
                                                answerObject.command = "sendToGameSession";
                                                answerObject.action = "playerDecideMovement";
                                                answerObject.sessionID = this.gameSession.sessionID;
                                                answerObject.message = "Deine gewürfelte Zahl geht über die Häusschenlänge hinaus";

                                                player.getConnection().sendUTF(JSON.stringify(answerObject));
                                            }
                                        }
                                        else{
                                            //Normaler Fortschitt

                                            gameFields[gameFieldId].owner = null;
                                            gameFields[gameFieldId + lastPlayerRoll].owner = player.getPlayerName();

                                            this.setNextPlayer();
                                        }
                                    }



                                }else{
                                    //Es ist ein eigener Spieler auf dem Feld mit der gewürfelten Reichweite. Suche eine andere Figur aus

                                    //Er soll nochmal entscheiden
                                    let answerObject = {};
                                    answerObject.command = "sendToGameSession";
                                    answerObject.action = "playerDecideMovement";
                                    answerObject.sessionID = this.gameSession.sessionID;
                                    answerObject.message = "Eine andere Figur von dir ist in der Reichweite schon plaziert";

                                    player.getConnection().sendUTF(JSON.stringify(answerObject));
                                }


                            }
                            else{
                                //Der Spieler hat keine Figur auf diesem Feld. Er soll nochmal entscheiden
                                let answerObject = {};
                                answerObject.command = "sendToGameSession";
                                answerObject.action = "playerDecideMovement";
                                answerObject.sessionID = this.gameSession.sessionID;

                                player.getConnection().sendUTF(JSON.stringify(answerObject));
                            }
                        }
                        else if(gameFieldType === 1){
                            //Es handelt sich um die Häusschen
                            if((gameFieldId + lastPlayerRoll) < player.getBasePoints().length){
                                //Der Wert befindet sich im Häusschen. Zug könnte ausfeführt werden
                                if(player.getBasePoints()[gameFieldId].owner === player.getPlayerName()){
                                    //Auf dem Feld befindet sich eine Figur. Alles gut
                                    player.getBasePoints()[gameFieldId].owner = null;
                                    player.getBasePoints()[gameFieldId + lastPlayerRoll].owner = player.getPlayerName();

                                    this.setNextPlayer();
                                }else{
                                    //Auf dem Feld befindet sich keine Figur. Fehler
                                    let answerObject = {};
                                    answerObject.command = "sendToGameSession";
                                    answerObject.action = "playerDecideMovement";
                                    answerObject.sessionID = this.gameSession.sessionID;
                                    answerObject.message = "Auf dem Feld befindet sich keine Figur. Fehler";
                                    player.getConnection().sendUTF(JSON.stringify(answerObject));
                                }

                            }else{
                                //Der Wert ist höher als die Häusschen-Länge. Fehler
                                let answerObject = {};
                                answerObject.command = "sendToGameSession";
                                answerObject.action = "playerDecideMovement";
                                answerObject.sessionID = this.gameSession.sessionID;
                                answerObject.message = "Der Wert ist höher als die Häusschen-Länge. Fehler";
                                player.getConnection().sendUTF(JSON.stringify(answerObject));
                            }
                        }

                        if(this.checkForGameWin(player)){
                            let answerObject = {};
                            answerObject.command = "sendToGameSession";
                            answerObject.action = "win";
                            answerObject.sessionID = this.gameSession.sessionID;
                            answerObject.message = player.getPlayerName() + " hat das Spiel gewonnen";


                            for (let i = 0; i < this.getPlayerList().length; i++) {
                                this.getPlayerList()[i].getConnection().sendUTF(JSON.stringify(answerObject));
                            }
                        }
                    }
                }else{
                    console.log("Spieler muss erst Würfeln");
                }

            }

        }


    }

    checkForGameWin(player){
        let win = true;
        for (let i = 0; i < player.getBasePoints().length; i++) {

            if(player.getBasePoints()[i].owner !== player.getPlayerName()){
                win = false;
            }
        }
        return win;
    }

    playerCanDoAnything(player, roll){
        let canDoField = this.playerCanDoAnythingOnGameField(player, roll);
        let canDoHome = this.playerCanDoAnythingOnBaseField(player, roll);

        if(canDoField || canDoHome){
            return true;
        }else{
            return false;
        }
    }

    playerCanDoAnythingOnGameField(player, roll){
        let canDo = false;
        for (let i = 0; i < gameFields.length; i++) {
            if(gameFields[i].owner === player.getPlayerName()){
                let gameFieldId = i;


                //Prüfen ob eine Figur, die am Startpunkt liegt, bewegt werden muss
                if(gameFields[player.getStartPoint()].owner === player.getPlayerName()){
                    //Ja, muss es
                    if(gameFieldId === player.getStartPoint()){
                        //Gültiger Zug

                        if(gameFields[gameFieldId + roll].owner !== player.getPlayerName()) {
                            canDo = true;
                        }
                    }
                }
                else{
                        //Es handelt sich um das Spielfeld

                            //Prüfen ob Spieler, eine Figur in Reichweite seines Zuges hat


                            let calculatedField = 0;
                            if((gameFieldId + roll) > gameFieldSize - 1){
                                calculatedField = (gameFieldId + roll) - gameFieldSize;
                            }else{
                                calculatedField = (gameFieldId + roll);
                            }

                            console.log("Prüfung: " + gameFieldId + "/" + roll + "/" + gameFieldSize + ": " + player.getBaseEntryPoint() + ": "+ player.getStartPoint());

                            if(gameFields[calculatedField].owner !== player.getPlayerName()){
                                //Es ist kein eigener Spieler auf dem Feld mit der gewürfelten Reichweite

                                if(player.getStartPoint() !== 0){
                                    //Gilt für alle Spieler außer Spieler 1

                                    if(calculatedField > player.getBaseEntryPoint() && gameFieldId < player.getStartPoint()){

                                        if(calculatedField - player.getBaseEntryPoint() <= 4){
                                            //Figur landet im Häusschen
                                            if(player.getBasePoints()[(calculatedField - player.getBaseEntryPoint()) - 1].owner == null){
                                                //Keine andere Figur ist auf dem Feld
                                                canDo = true;
                                            }
                                        }
                                    }
                                    else{
                                        //Normaler Fortschitt
                                        canDo = true;
                                    }
                                }
                                else{
                                    //Gilt für Spieler 1
                                    let calculatedField = (gameFieldId + roll);//43

                                    if(calculatedField > player.getBaseEntryPoint() && gameFieldId > player.getStartPoint()){
                                        //Figur müsste ins Häusschen. Prüfen ob gewürfelte Zahl zu hoch ist

                                        if((calculatedField - player.getBaseEntryPoint() <= 4)){
                                            //Figur landet im Häusschen

                                            //prüfen ob sich bereits eine Figur auf dem Feld befindet
                                            if(player.getBasePoints()[(calculatedField - player.getBaseEntryPoint()) - 1].owner == null){
                                                //Keine andere Figur ist auf dem Feld
                                                canDo = true;
                                            }
                                        }
                                    }
                                    else{
                                        canDo = true;
                                    }
                                }



                            }
                }
            }
        }

        return canDo;
    }

    playerCanDoAnythingOnBaseField(player, roll){
        let canDo = false;
        for (let i = 0; i < player.getBasePoints().length; i++) {
            if(player.getBasePoints()[i].owner !== null){
                //Prüfen ob in der Playerbase noch ein Zug durchgeführt werden kann
                if(i + roll < player.getBasePoints().length){
                    if(player.getBasePoints()[i + roll].owner === null){
                        canDo = true;
                        break;
                    }
                }
            }


        }
        return canDo;
    }

    countPlayerBaseFigures(player){
        let count = 0;
        for (let i = 0; i < player.getBasePoints().length; i++) {
            if(player.getBasePoints()[i].owner !== null){
                count++;
            }
        }
        return count;
    }

    checkBasePossibleRolls(player){
        let figuresInBaseCount = this.countPlayerBaseFigures(player);

        let checkBasePossibleRoll = false;
        for (let i = player.getBasePoints().length; i > player.getBasePoints().length - figuresInBaseCount; i--) {
            //Prüfen ob in der Playerbase noch ein Zug durchgeführt werden kann
            if(player.getBasePoints()[i-1].owner === null){
                checkBasePossibleRoll = true;
                break;
            }
        }

        return checkBasePossibleRoll;
    }

    setNextPlayer(){
        //Nachricht an alle Spieler, dass sich das Spielfeld verändert hat
        this.sendPlayersGameFieldUpdate();

        if((this.playerlist.length-1) === this.currentPlayer){
            this.currentPlayer = 0;
        }else{
            this.currentPlayer++;
        }

        currentPlayerRolls = 0;
        playerMustRoll = true;
        console.log("Nächster Spieler ist am Zug");

        this.sendRollCommand();

    }

    sendRollCommand(){
        let answerObject = {};
        answerObject.command = "sendToGameSession";
        answerObject.action = "roll";
        answerObject.sessionID = this.gameSession.sessionID;
        let p = this.getPlayerList()[this.currentPlayer];
        answerObject.playername = p.getPlayerName();

        for (let i = 0; i < this.getPlayerList().length; i++) {
            this.getPlayerList()[i].getConnection().sendUTF(JSON.stringify(answerObject));
        }
    }

    getPlayerList(){
        return this.playerlist
    }

    initPlayer(){
        //for (let i = 0; i <= playerlist.length; i++) {
        console.log(this.playerlist.length + " Spieler spielen");
        for (let i = 0; i < this.playerlist.length; i++) {
            this.playerlist[i].createBasePoints(4);

            /*this.playerlist[i].getBasePoints()[0].owner = this.playerlist[i].getPlayerName();
            this.playerlist[i].getBasePoints()[1].owner = this.playerlist[i].getPlayerName();
            this.playerlist[i].getBasePoints()[2].owner = this.playerlist[i].getPlayerName();
            this.playerlist[i].getBasePoints()[3].owner = this.playerlist[i].getPlayerName();*/

            //Setzen der HomePosition für jeden Spieler
            let playerStartPoint = i * mapSizePart;
            this.playerlist[i].setStartPoint(playerStartPoint);

            if(playerStartPoint !== 0){
                this.playerlist[i].setBaseEntryPoint(playerStartPoint - 1);
            }else{
                this.playerlist[i].setBaseEntryPoint(gameFieldSize - 1);
            }

            console.log("Player 1: HomePoint: " + this.playerlist[i].getStartPoint() + " BaseEntry: " + this.playerlist[i].getBaseEntryPoint());
        }

        console.log("Player initialized");
    }

    initGameField(){
        gameFields = [];
        for (let i = 0; i < gameFieldSize; i++) {
            let gameField = {};
            gameField.owner = null;
            gameFields.push(gameField);
        }
        console.log("GameField initialized");
    }

    selectRandomPlayer(){
        //this.currentPlayer = Math.round(Math.random() *  (this.playerlist.length-1));
        this.currentPlayer = 0;
        return this.playerlist[this.currentPlayer];
    }

    roll(){
        return Math.round(Math.random() * 6.999);
        //return 6
    }

    sendPlayersGameFieldUpdate(){
        let answerObject = {};
        answerObject.command = "sendToGameSession";
        answerObject.action = "sendPlayersGameFieldUpdate";
        answerObject.playersize = this.getPlayerList().length;
        answerObject.gameFieldData = gameFields;
        answerObject.playersHomeBases = [];

        for (let i = 0; i < this.getPlayerList().length; i++) {
            let playerHome = {};
            playerHome.playerName = this.getPlayerList()[i].getPlayerName();
            playerHome.mapFieldSize = gameFieldSize;
            playerHome.baseEntryPoint = this.getPlayerList()[i].getBaseEntryPoint();
            playerHome.startPoint = this.getPlayerList()[i].getStartPoint();
            playerHome.playerBase = this.getPlayerList()[i].getBasePoints();
            answerObject.playersHomeBases.push(playerHome);
        }

        for (let i = 0; i < this.getPlayerList().length; i++) {
            this.getPlayerList()[i].getConnection().sendUTF(JSON.stringify(answerObject));
        }
    }

    countPlayersFiguresOnGameField(player){
        let count = 0;
        for (let i = 0; i < gameFields.length; i++) {
            if(gameFields[i].owner === player.getPlayerName()){
                count++;
            }
        }

        for (let i = 0; i < player.getBasePoints().length; i++) {
            if(player.getBasePoints()[i].owner === player.getPlayerName()){
                count++;
            }
        }

        return count;
    }

}

