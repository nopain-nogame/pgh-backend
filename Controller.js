#!/usr/bin/env node

let WebSocketServer = require('./node_modules/websocket').server;
let http = require('http');
//const GameSession = require("./GameSession");
let GameSession = require("./GameSession");
let PermissionType = require("./PermissionType");

let server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});
server.listen(8008, function() {
    console.log((new Date()) + ' Server is listening on port 8008');
});

let wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});

let connections = [];

//Hier werden aktive GameSession gespeichert. Durch diese Sessions handelt der Server entsprechend, den Nachrichten des Clients
let gameSessions = [];

wsServer.on('request', function(request) {

    let connection = request.accept('echo-protocol', request.origin);

    console.log((new Date()) + ' Connection accepted.');
    connection.on('message', function(message) {

        if (message.type === 'utf8') {
            console.log(message.utf8Data);
            try {
                JSON.parse(str);
                let receivementData = JSON.parse(message.utf8Data);

                let sendPlayersOnSessionDataChanged = false;

                //for (let i = 0; i < receivementData.length; i++) {
                let command = receivementData.command;
                if(command === "login"){
                    let answerObject = {};
                    if(!isUserAlreadyConnected(connection)){
                        connections.push(connection);

                        answerObject.command = "login";
                        answerObject.status = true;

                    }else{
                        answerObject.command = "login";
                        answerObject.status = true;
                        answerObject.reason = "Bereits angemeldet";
                        console.log("Ein Nutzer hat sich versucht doppelt anzumelden");
                    }

                    connection.sendUTF(JSON.stringify(answerObject));
                }
                else if(command === "createSession"){

                    let sessionName = receivementData.sessionName;
                    let playerName = receivementData.username;

                    let answerObject = {};
                    //Nutzer möchte eine Session erstellen
                    if(!doesSessionExistByName(sessionName)){
                        let gameSession = new GameSession(this, sessionName);
                        console.log("Session " + gameSession.getSessionName() + " wurde erstellt. Session ID: " + gameSession.getSessionID() );
                        //Der Sessionersteller wird als Spieler erstellt und zum Session-Admin gesetzt
                        gameSession.addPlayer(playerName, connection);
                        gameSessions.push(gameSession);

                        answerObject.command = "createSession";
                        answerObject.status = true;
                        answerObject.sessionID = gameSession.getSessionID();
                        answerObject.sessionPassword = gameSession.getSessionPassword();
                    }else{
                        console.log("Session existiert bereits");
                        answerObject.command = "createSession";
                        answerObject.status = false;
                        answerObject.reason = "Eine Session mit diesem Namen besteht bereits";
                    }
                    connection.sendUTF(JSON.stringify(answerObject));
                }
                else if(command === "joinSession"){
                    let answerObject = {};
                    //Nutzer möchte einer Session beitreten
                    let playerName = receivementData.username;
                    let sessionID = receivementData.sessionID;
                    let sessionPassword = receivementData.sessionPassword;

                    if(doesSessionExistById(sessionID)){
                        let gameSession = getGameSession(sessionID);

                        if(sessionPassword === gameSession.getSessionPassword()){
                            if(gameSession.addPlayer(playerName, connection)){
                                //Spieler wurde hinzugefügt.
                                answerObject.command = "joinSession";
                                answerObject.status = true;

                                sendPlayersOnSessionDataChanged = true;
                            }else{
                                //Spieler wurde nicht hinzugefügt. Benutzername vergeben oder Lobby voll
                                answerObject.command = "joinSession";
                                answerObject.status = false;
                                answerObject.reason = "Der Benutzername ist bereits vergeben oder die Lobby ist voll";
                            }
                        }else{
                            answerObject.command = "joinSession";
                            answerObject.status = false;
                            answerObject.reason = "Das Sessionpasswort ist falsch";
                        }


                    }else{
                        console.log("Session existiert nicht");
                        answerObject.command = "joinSession";
                        answerObject.status = false;
                        answerObject.reason = "Diese Session exisitiert nicht";
                    }
                    connection.sendUTF(JSON.stringify(answerObject));

                    if(sendPlayersOnSessionDataChanged){
                        let session = getGameSession(sessionID);
                        if(session !== null){
                            session.notifyPlayersOnSessionDataChanged();
                        }
                    }
                }
                else if(command === "setUsername"){
                    let answerObject = {};

                    let sessionID = receivementData.sessionID;
                    let username = receivementData.username;

                    let gameSession = getGameSession(sessionID);
                    if(gameSession !== null){
                        let foundUser = false;
                        let playerList = gameSession.getPlayerList();
                        for (let j = 0; j < playerList.length; j++) {
                            if(playerList[j].getConnection() === connection){
                                foundUser = true;
                                if(!gameSession.doesPlayerExist(username)){
                                    playerList[j].setUsername(username);

                                    answerObject.command = "setUsername";
                                    answerObject.status = true;
                                    answerObject.reason = "Der Benutzername wurde erfolgreich geändert";

                                    sendPlayersOnSessionDataChanged = true;
                                }else{
                                    answerObject.command = "setUsername";
                                    answerObject.status = false;
                                    answerObject.reason = "Der Benutzername existiert bereits";
                                }
                                break;
                            }
                        }

                        if(!foundUser){
                            answerObject.command = "setUsername";
                            answerObject.status = false;
                            answerObject.reason = "Der Benutzer konnte nicht gefunden werden";
                        }

                    }else{
                        //Die angegebene Gamesession existiert nicht
                        answerObject.command = "setUsername";
                        answerObject.status = false;
                        answerObject.reason = "Die Session existiert nicht";
                    }

                    connection.sendUTF(JSON.stringify(answerObject));

                    if(sendPlayersOnSessionDataChanged){
                        let session = getGameSession(sessionID);
                        if(session !== null){
                            session.notifyPlayersOnSessionDataChanged();
                        }
                    }
                }
                else if(command === "setSessionGame"){
                    let answerObject = {};

                    let sessionID = receivementData.sessionID;
                    let sessionGame = receivementData.sessionGame;

                    let gameSession = getGameSession(sessionID);
                    if(gameSession !== null){
                        if(gameSession.getPlayerList()[0].getConnection() === connection){
                            gameSession.setSessionGame(sessionGame);

                            answerObject.command = "setSessionGame";
                            answerObject.status = true;
                            answerObject.reason = "Das Spiel wurde für die Session gesetzt";
                        }else{
                            //Die angegebene Gamesession existiert nicht
                            answerObject.command = "setSessionGame";
                            answerObject.status = false;
                            answerObject.reason = "Du bist nicht der Session-Admin";
                        }
                    }else{
                        //Die angegebene Gamesession existiert nicht
                        answerObject.command = "setSessionGame";
                        answerObject.status = false;
                        answerObject.reason = "Die Session existiert nicht";
                    }

                    connection.sendUTF(JSON.stringify(answerObject));
                }
                else if(command === "setPlayerReadyState"){
                    let answerObject = {};

                    let sessionID = receivementData.sessionID;
                    let playerReadyState = receivementData.readyState;

                    let gameSession = getGameSession(sessionID);
                    if(gameSession !== null){
                        let foundUser = false;
                        let playerList = gameSession.getPlayerList();
                        for (let j = 0; j < playerList.length; j++) {
                            if(playerList[j].getConnection() === connection){
                                playerList[j].setReadyState(playerReadyState);
                                foundUser = true;
                                answerObject.command = "setPlayerReadyState";
                                answerObject.status = true;
                                answerObject.reason = "Der Spieler Status wurde gesetzt";

                                if(gameSession.checkForGameStart()){
                                    console.log("Alle Spieler bereit. Es geht los");

                                    answerObject.command = "gameStart";
                                    answerObject.reason = "Das Spiel geht los";

                                    for (let i = 0; i < playerList.length; i++) {
                                        playerList[i].connection.sendUTF(JSON.stringify(answerObject));
                                    }

                                    if(gameSession.createGame()){
                                        console.log("GameSession gestartet");
                                    }
                                }else{
                                    sendPlayersOnSessionDataChanged = true;
                                    console.log("Entweder sind zu wenige oder zu viele Spieler in der Session. Oder nicht alle Spieler sind bereit.");
                                }
                                break;
                            }
                        }

                        if(!foundUser){
                            answerObject.command = "setPlayerReadyState";
                            answerObject.status = false;
                            answerObject.reason = "Der Benutzer konnte nicht gefunden werden";
                        }

                    }else{
                        //Die angegebene Gamesession existiert nicht
                        answerObject.command = "setPlayerReadyState";
                        answerObject.status = false;
                        answerObject.reason = "Die Session existiert nicht";
                    }

                    connection.sendUTF(JSON.stringify(answerObject));

                    if(sendPlayersOnSessionDataChanged){
                        let session = getGameSession(sessionID);
                        if(session !== null){
                            session.notifyPlayersOnSessionDataChanged();
                        }
                    }
                }
                else if(command === "sendToGameSession"){
                    let answerObject = {};

                    let sessionID = receivementData.sessionID;

                    let gameSession = getGameSession(sessionID);
                    if(gameSession !== null){
                        if(gameSession.getGameHandler() !== null){
                            //Leite die Informationen an den GameHandler weiter
                            gameSession.getGameHandler().onMessageReceived(receivementData, connection);
                        }else{
                            //Die Spiel hat noch nicht gestartet
                            answerObject.command = "sendToGameSession";
                            answerObject.status = false;
                            answerObject.reason = "Das Spiel hat noch nicht begonnen";
                        }
                    }else{
                        //Die angegebene Gamesession existiert nicht
                        answerObject.command = "sendToGameSession";
                        answerObject.status = false;
                        answerObject.reason = "Die Session existiert nicht";
                    }
                }
                else if(command === "kick"){
                    let answerObject = {};

                    let sessionID = receivementData.sessionID;
                    let username = receivementData.username;

                    if(doesSessionExistById(sessionID)){
                        let gameSession = getGameSession(sessionID);
                        let isAdmin = false;
                        for (let i = 0; i < gameSession.getPlayerList().length; i++) {
                            if(gameSession.getPlayerList()[i].getConnection() === connection){
                                if(gameSession.getPlayerList()[i].getPermission() === PermissionType.admin){
                                    isAdmin = true;
                                }
                                break;
                            }
                        }

                        if(isAdmin){
                            if(gameSession.kickPlayerByUsername(username, "Du wurdest aus der Session gekicked")){
                                gameSession.generateSessionPassword(7);
                                answerObject.command = "kick";
                                answerObject.status = true;
                                answerObject.sessionPassword = gameSession.getSessionPassword();
                                answerObject.reason = "Der Spieler wurde von der GameSession entfernt";

                                gameSession.notifyPlayersOnSessionDataChanged();

                            }else{
                                answerObject.command = "kick";
                                answerObject.status = false;
                                answerObject.reason = "Der Spieler konnte nicht entfernt werden";
                            }
                        }else{
                            answerObject.command = "kick";
                            answerObject.status = false;
                            answerObject.reason = "Du hast keine Berechtigung dazu";
                        }
                    }else{
                        //Die angegebene Gamesession existiert nicht
                        answerObject.command = "kick";
                        answerObject.status = false;
                        answerObject.reason = "Die Session existiert nicht";
                    }

                    connection.sendUTF(JSON.stringify(answerObject));

                }
                else if(command === "sessionEnd"){
                    let answerObject = {};

                    let sessionID = receivementData.sessionID;

                    if(doesSessionExistById(sessionID)){
                        let gameSession = getGameSession(sessionID);
                        let isAdmin = false;
                        for (let i = 0; i < gameSession.getPlayerList(); i++) {
                            if(gameSession.getPlayerList()[i].getConnection() === connection){
                                if(gameSession.getPlayerList()[i].getPermission() === PermissionType.admin){
                                    isAdmin = true;
                                    break;
                                }
                            }
                        }

                        if(isAdmin){

                            for (let i = 0; i < gameSession.getPlayerList().length; i++) {
                                gameSession.kickPlayerByUsername(gameSession.getPlayerList().getPlayerName(), "Die Session wurde beendet");
                            }

                            closeGameSessionIfEmpty(sessionID);
                        }else{
                            answerObject.command = "sessionEnd";
                            answerObject.status = false;
                            answerObject.reason = "Du hast keine Berechtigung dazu";
                        }
                    }else{
                        //Die angegebene Gamesession existiert nicht
                        answerObject.command = "sessionEnd";
                        answerObject.status = false;
                        answerObject.reason = "Die Session existiert nicht";
                    }
                    connection.sendUTF(JSON.stringify(answerObject));

                }
            } catch (e) {
                console.log("Kein JSON mitgeliefert. Nachricht wird ignoriert");
            }



        }
    });
    connection.on('close', function(reasonCode, description) {

        console.log("Jemand hat das Spiel verlassen: " + reasonCode + " " + description);

        //Client aus allen Sessions entfernen
        for (let i = 0; i < gameSessions.length; i++) {
            for (let j = 0; j < gameSessions[i].getPlayerList().length; j++) {
                let player = gameSessions[i].getPlayerList()[j];

                if(player.getConnection() === connection){
                    gameSessions[i].removePlayer(player.getPlayerName());

                    //Prüfen ob die GameSession geschlossen werden muss, wenn keine Spieler mehr in der Lobby sind
                    closeGameSessionIfEmpty(gameSessions[i].getSessionID());
                    break;
                }
            }
        }

        removeClient(connection);

    });
});

function isUserAlreadyConnected(connection){

    for (let i = 0; i < connections.length; i++) {
        if(connections[i] === connection){
            console.log("Nutzer bereits eingeloggt");
            return true;
        }

        return false;
    }

}

function removeClient(connection){

    for (let i = 0; i < connections.length; i++) {
        if(connections[i] === connection){
            connections.splice(i, 1);
            return true;
        }
        return false;
    }
}

function doesSessionExistByName(sessionName){
    for (let i = 0; i < gameSessions.length; i++) {
        if(gameSessions[i].getSessionName() === sessionName){
            return true;
        }
    }
    return false;
}

function doesSessionExistById(sessionID){
    for (let i = 0; i < gameSessions.length; i++) {
        if(gameSessions[i].getSessionID() === sessionID){
            return true;
        }
    }
    return false;
}

function getGameSession(sessionID){
    for (let i = 0; i < gameSessions.length; i++) {
        if(gameSessions[i].getSessionID() === sessionID){
            return gameSessions[i];
        }
    }
    return null;
}

function closeGameSessionIfEmpty(sessionID){
    for (let i = 0; i < gameSessions.length; i++) {
        if(gameSessions[i].getSessionID() === sessionID){
            if(gameSessions[i].getPlayerList().length === 0){
                console.log("GameSession " + sessionID + " wird geschlossen");
                gameSessions.splice(i, 1);
            }
        }
    }
}