#!/usr/bin/env node
let WebSocketClient = require('websocket').client;

let client = new WebSocketClient();

let isLoggedIn = false;


let readline = require('readline');
let log = console.log;

let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

//we have to actually start our recursion somehow

/*
Test-Befehle

{"command": "setUsername","sessionID": 331,"username": "Leon"}

{"command": "setPlayerReadyState","sessionID": 0,"readyState": true}

{"command": "sendToGameSession","action": "roll","sessionID": 0,"readyState": true}

{"command": "sendToGameSession","action": "playerDecideMovement", "gameFieldType": 0, "sessionID": 0,"playerGameFieldId": 39}



 */



client.on('connectFailed', function(error) {
    console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {
    sendNumber(connection);

    console.log('WebSocket Client Connected');
    connection.on('error', function(error) {
        console.log("Connection Error: " + error.toString());
    });
    connection.on('close', function() {
        console.log('echo-protocol Connection Closed');
    });
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            let receivementData = JSON.parse(message.utf8Data);
            console.log(receivementData);

            if(receivementData.playersHomeBases !== undefined){
                for (let i = 0; i < receivementData.playersHomeBases.length; i++) {
                    console.log(receivementData.playersHomeBases[i].playerBase);
                }

            }

            let command = receivementData.command;

            if(command === "login"){
                if(receivementData.status === true){
                    isLoggedIn = true;
                    let recursiveAsyncReadLine = function () {
                        rl.question('Command: ', function (answer) {
                            if (answer == 'exit') //we need some base case, for recursion
                                return rl.close(); //closing RL and returning from function.

                            let json = answer;
                            connection.sendUTF(json);

                            recursiveAsyncReadLine(); //Calling this function again to ask new question
                        });
                    };
                    recursiveAsyncReadLine();
                }else{
                    console.log(receivementData.reason);
                }

            }
            /*else if(command === "createSession"){
                //Testweise wird beim erfolgreichen erstellen einer Session, anschließend der Benutzername geändert
                /*answerObject = {};
                answerObject.command = "setUsername";
                answerObject.sessionID = receivementData.sessionID;
                answerObject.username = "TestUser";
                connection.sendUTF(JSON.stringify(answerObject));/

                answerObject = {};
                answerObject.command = "setPlayerReadyState";
                answerObject.sessionID = receivementData.sessionID;
                answerObject.readyState = true;
                connection.sendUTF(JSON.stringify(answerObject));

            }else if(command === "sendToGameSession"){
                answerObject = {};
                answerObject.command = "sendToGameSession";
                answerObject.action = "roll";
                answerObject.sessionID = receivementData.sessionID;
                answerObject.readyState = true;
                connection.sendUTF(JSON.stringify(answerObject));
            }*/

        }
    });

});

client.connect('ws://localhost:8008/', 'echo-protocol');


function sendNumber(connection) {
    if (connection.connected) {
        let answerObject = {};
        answerObject.command = "login";
        connection.sendUTF(JSON.stringify(answerObject));

        answerObject = {};
        answerObject.command = "joinSession";
        answerObject.sessionID = 0;
        answerObject.sessionPassword = "test";
        answerObject.username = "Spieler 2";
        connection.sendUTF(JSON.stringify(answerObject));

    }
}