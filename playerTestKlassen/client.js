#!/usr/bin/env node
let WebSocketClient = require('websocket').client;

let client = new WebSocketClient();

let isLoggedIn = false;

client.on('connectFailed', function(error) {
    console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {
    sendNumber(connection);

    console.log('WebSocket Client Connected');
    connection.on('error', function(error) {
        console.log("Connection Error: " + error.toString());
    });
    connection.on('close', function() {
        console.log('echo-protocol Connection Closed');
    });
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            let receivementData = JSON.parse(message.utf8Data);
            console.log(receivementData);

            let command = receivementData.command;

            if(command === "login"){
                if(receivementData.status === true){
                        isLoggedIn = true;
                }else{
                    console.log(receivementData.reason);
                }

            }else if(command === "createSession"){
                //Testweise wird beim erfolgreichen erstellen einer Session, anschließend der Benutzername geändert
                /*answerObject = {};
                answerObject.command = "setUsername";
                answerObject.sessionID = receivementData.sessionID;
                answerObject.username = "TestUser";
                connection.sendUTF(JSON.stringify(answerObject));*/

                answerObject = {};
                answerObject.command = "setPlayerReadyState";
                answerObject.sessionID = receivementData.sessionID;
                answerObject.readyState = true;
                connection.sendUTF(JSON.stringify(answerObject));

            }else if(command === "sendToGameSession"){
                answerObject = {};
                answerObject.command = "sendToGameSession";
                answerObject.action = "roll";
                answerObject.sessionID = receivementData.sessionID;
                answerObject.readyState = true;
                connection.sendUTF(JSON.stringify(answerObject));
            }

        }
    });

});

client.connect('ws://localhost:8008/', 'echo-protocol');


function sendNumber(connection) {
    if (connection.connected) {
        let answerObject = {};
        answerObject.command = "login";
        connection.sendUTF(JSON.stringify(answerObject));

        answerObject = {};
        answerObject.command = "createSession";
        answerObject.sessionName = "Test";
        answerObject.sessionGame = "Mensch Ärgere dich nicht";
        answerObject.username = "admin";
        connection.sendUTF(JSON.stringify(answerObject));

    }
}