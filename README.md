# README #

Klassendiagramm: https://app.diagrams.net/#G1NQdxlDU8fBGWI4vqKNonwGNOJxw7o-a8

### Funktionale Anforderungen ###

#### Web-Anwendung ####

| Funktion	|	Beschreibung																																		|
| ----------|----------------------------------------------------------------------------------------------------------------------------------------------------	|
| LF 10		|	Spieler sollen über die Web-Anwendung auf Spiele zugreifen können																					|
| LF 20		|	Spieler sollen einen eigenen Benutzernamen erstellen können																							|
| LF 30		|	Spieler sollen ihren Benutzernamen ändern können.																									|
| LF 40		|	Spieler sollen eine Session für eine bestimmte Anzahl an Spielern erstellen können																	|
| LF 50		|	Host-Spieler sollen nach dem Erstellen einer Session eine ID einsehen und kopieren können, mit der andere Spieler der Session beitreten können.		|
| LF 60		|	Spieler sollen über die Web-Anwendung dem Host beitreten können 																					|
| LF 70		|	Spieler sollen alle anderen Teilnehmer einer Session in einer Liste sehen können.																	|
| LF 80		|	Spieler sollen das aktuell in der Session ausgewählte Spiel einsehen können																			|
| LF 90		|	Spieler sollen vor einem Spiel angeben müssen, ob sie bereit sind.																					|
| LF 100	|	Der Host-Spieler sollen ein Spiel starten können. 																									|
| LF 110	|	Der Host-Spieler soll andere Spieler in einer Session entfernen können.																				|
| LF 120	|	Spieler sollen die Session jederzeit verlassen können.																								|
| LF 130	|	Wenn der Host-Spieler die Session verlässt, soll ein neuer Host bestimmt werden.																	|
| LF 140	|	Der Host-Spieler soll ein Spiel jederzeit beenden können.    																						|
| LF 150	|	Der Host-Spieler soll eine Session jederzeit beenden können. (Dies soll das Beenden des laufenden Spieles mit einschließen)							|
| LF 160	|	Der Host-Spieler soll ein Passwort für eine Session generieren und einsehen können, welches mit jedem entfernten Spieler neu generiert wird.		|

#### Spielfunktionen (Allgemein) ####
 
| Funktion	|	Beschreibung																																		|
| ----------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| LF 170	|	Performance-Einstellungen ändern																													|
| LF 180	|	Feedback falls die Interaktion des Spielers nicht Spiel-konform ist																					|
| LF 190	|	Wenn eine KI im Spiel integriert ist, soll diese einen Spieler übernehmen, falls dieser das Spiel verlässt											|

#### Basisspiel (Mensch ärgere dich nicht) ####

| Funktion	|	Beschreibung																																		|
| ----------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| LF 200	|	Würfeln																																				|
| LF 210	|	Spielfigur wählen, die den Zug ausführen soll																										|
