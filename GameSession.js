let Player = require("./Player");
let PermissionType = require("./PermissionType");
let GameHandler = require("./MADN/GameHandler");

let playerlist = [];

let maxPlayer = 2;
let minPlayer = 1;

let gameHandler;


module.exports = class GameSession{

    constructor(controller, sessionName) {
        this.controller = controller;
        this.sessionName = sessionName;
        this.sessionID = this.generateSessionID();
        //this.sessionPassword = this.generateSessionPassword(7);
        this.sessionPassword = "test"
    };

    setSessionName(sessionName){
        this.sessionName = sessionName;
    }

    getSessionName(){
        return this.sessionName;
    }

    getSessionID(){
        return this.sessionID;
    }

    setSessionGame(sessionGame){
        this.sessionGame = sessionGame;
    }

    getSessionGame(){
        return this.sessionGame;
    }

    addPlayer(playerName, connection){

        if(!(this.getPlayerList().length + 1 > maxPlayer)){
            //Die lobby hat noch Platz für Spieler frei
            if(!this.doesPlayerExist(playerName)){
                let player = new Player(playerName, connection);
                if(this.getPlayerList().length === 0){
                    player.setPermission(PermissionType.admin);
                    console.log(player.getPlayerName() + " wurde zum Admin gesetzt" + PermissionType.admin);
                }
                playerlist.push(player);
                console.log("Spieler wurde in Session hinzugefügt");
                return true;
            }else{
                console.log("Fehler Spieler Existiert bereits");
                return false;
            }
            return false;
        }else{
            //Die lobby ist bereits voll
            return false;
        }
    }

    removePlayer(playerName){

        for (let i = 0; i < playerlist.length; i++) {
            if(playerlist[i].getPlayerName() === playerName){

                let wasAdmin = false;
                if(playerlist[i].getPermission() === PermissionType.admin){
                    wasAdmin = true;
                }

                playerlist.splice(i, 1);

                if(wasAdmin){
                    if(playerlist.length > 0){
                        playerlist[0].setPermission(PermissionType.admin);
                    }
                }

                this.notifyPlayersOnSessionDataChanged();
                break;
            }
        }
    }

    getPlayerList(){
        return playerlist;
    }

    doesPlayerExist(playerName){
        for (let i = 0; i < playerlist.length; i++) {
            if(playerlist[i].getPlayerName() === playerName){
                return true;
            }else{
                return false;
            }
        }
    }

    generateSessionID(){
        //Vorerst wird die SessionID random erzeugt. Es muss geprüft werden ob die ID schon bereits vergeben ist.
        //return Math.random() * 1000;
        //return Math.round(Math.random() * 1000);
        return 0;
    }

    setMaxPlayer(player){
        this.maxPlayer = player;
    }

    getMaxPlayer(){
        return maxPlayer;
    }

    setMinPlayer(player){
        this.minPlayer = player;
    }

    getMinPlayer(){
        return minPlayer;
    }

    notifyPlayersOnSessionDataChanged(){
        //Alle Spieler erhalten Daten über alle andere Spieler, da sich Informationen von einem oder mehreren Spielern / Session geändert haben / hat.
        let changedData = this.fetchSessionData();
        let playerList = this.getPlayerList();
        for (let i = 0; i < playerList.length; i++) {
            playerList[i].getConnection().sendUTF(changedData);
        }
    }

    fetchSessionData(){
        let answer = {};
        answer.command = "onSessionDataChanged";
        answer.sessionID = this.getSessionID();
        answer.sessionName = this.getSessionName();
        answer.sessionGame = this.sessionGame;
        answer.sessionMaxPlayer = this.getMaxPlayer();
        answer.sessionMinPlayer = this.getMinPlayer();

        let playerData = [];

        let playerList = this.getPlayerList();
        let playerInfo;
        for (let i = 0; i < playerList.length; i++) {
            playerInfo = {};
            playerInfo.playerName = playerList[i].getPlayerName();
            playerInfo.playerPermission = playerList[i].getPermission();
            playerInfo.playerReadyState = playerList[i].getReadyState();
            playerData.push(playerInfo);
        }

        answer.playerData = playerData;

        return JSON.stringify(answer);
    }

    checkForGameStart(){
        if(playerlist.length >= minPlayer && playerlist.length <= maxPlayer){
            let readyCount = 0;
            for (let i = 0; i < playerlist.length; i++) {
                if(playerlist[i].getReadyState() === true){
                    readyCount += 1;
                }
            }

            if(readyCount === playerlist.length){
                return true;
            }else{
                console.log("Nicht alle Spieler sind bereit (" + readyCount + "/" + playerlist.length + ")");
                return false;
            }
        }else{
            //Entweder sind zu wenige oder zu viele Spieler in der Session
            return false;
        }
    }

    createGame(){
        if(gameHandler == null){
            gameHandler = new GameHandler(this, this.getPlayerList());
            //console.log(this.getGameHandler());
            return true;
        }
        return false;
    }


    getGameHandler(){
        return gameHandler;
    }

    generateSessionPassword(length){
        let result           = '';
        let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for ( let i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    getSessionPassword(){
        return this.sessionPassword;
    }

    kickPlayerByUsername(username, message){
        for (let i = 0; i < playerlist.length; i++) {
            if(playerlist[i].getPlayerName() === username){

                let answerObject = {};
                answerObject.command = "kicked";
                answerObject.reason = message;
                playerlist[i].getConnection().sendUTF(JSON.stringify(answerObject));

                playerlist.splice(i, 1);
                return true;
            }
        }
        return false;
    }
}

