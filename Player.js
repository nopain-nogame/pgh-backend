let PermissionType = require("./PermissionType");

module.exports = class Player {
    constructor(playerName, connection) {
        this.playerName = playerName;
        this.connection = connection;
        this.permissionType = PermissionType.player;
        this.readyState = false;
    }

    getConnection(){
        return this.connection;
    }

    getPlayerName(){
        return this.playerName;
    }

    getPermission(){
        return this.permissionType;
    }

    setPermission(permissionType){
        this.permissionType = permissionType;
    }

    setUsername(username){
        this.playerName = username;
    }

    setReadyState(readyState){
        this.readyState = readyState;
    }

    getReadyState(){
        return this.readyState;
    }

    //Wichtige Funkionen für den Spielverlauf

    setRolls(rolls){
        this.rolls = rolls;
    }

    getRolls(){
        return this.rolls;
    }

    setStartPoint(startPoint){
        this.startPoint = startPoint;
    }

    getStartPoint(){
        return this.startPoint;
    }

    setBaseEntryPoint(baseEntryPoint){
        this.baseEntryPoint = baseEntryPoint;
    }

    getBaseEntryPoint(){
        return this.baseEntryPoint;
    }

    createBasePoints(size){
        this.basePoints = [];

        for (let i = 0; i < size; i++) {
            let field = {};
            field.owner = null;
            this.basePoints.push(field);
        }
    }

    getBasePoints(){
        return this.basePoints;
    }
}